# -*- coding: utf-8 -*-
"""
Created on Fri Apr 12 10:57:25 2019

@author: CWDSG05

Script updated for converting S3 to OSS migration
"""

import oss2
import os


class s3_conn(object):
	def __init__(self):
		self.access_key_id = 'LTAI2LsL385C5pwy'
		self.access_key_secret = "3LL6IsuiXUF7RTqNrl8IlVFdjLov38"
		#self.bucket_name = 'cwd-nexus'
		self.endpoint = 'http://oss-ap-southeast-5.aliyuncs.com' # Suppose that your bucket is in the Hangzhou region.
		self.myBucketName = 'cwd-nexus'
		self.bucket = oss2.Bucket(oss2.Auth(self.access_key_id, self.access_key_secret), self.endpoint, self.myBucketName)

	def down_frm_s3(self,remote_file_pathname):
		local_file_name = 'files/'+os.path.basename(remote_file_pathname).split('.')[0]+'_cp.'+os.path.basename(remote_file_pathname).split('.')[1]

		try:
			self.bucket.get_object_to_file(remote_file_pathname, local_file_name)
			print('File Downloaded successfully:',local_file_name)
			return local_file_name,200
		except oss2.exceptions.NoSuchKey as e:
			print('{0} not found: http_status={1}, request_id={2}'.format(remote_file_pathname, e.status, e.request_id))
			return None,404


	def upl_to_s3(self,myBucketName,local_file_name,remote_file_pathname,file_type='image'):
		'''#remote file pathname will be initial key path received from API and it will be managed based on file_type to upload automatically
	'''
		if file_type == 'image':  #it means it is from transformation script and needs to be uploaded on new_folder with initial filename altered
			'''#Work on changing the path of Upload
	'''
			rmt_file_path_split = remote_file_pathname.split('/')
			chg_rmt_file_path_split = [split if split!='documents' else 'ocr' for split in rmt_file_path_split]
			#print('split after:',chg_rmt_file_path_split)
			'''#work on changing the name of upload
	'''
			chg_rmt_file_path_split_nmChange = chg_rmt_file_path_split[-1].split('.')[0]+'_transform.jpg'  #Add _cut in last of image for upload
			chg_rmt_file_path_split[-1] = chg_rmt_file_path_split_nmChange
			chg_rmt_file_path_join = '/'.join(chg_rmt_file_path_split)

			#Upload file to S3 bucket
			if(os.path.exists(local_file_name)):
				try:
					self.bucket.put_object_from_file(chg_rmt_file_path_join, local_file_name)
					print("File:'",local_file_name,"' Uploaded successfully to path:",chg_rmt_file_path_join)
					return self.myBucketName,chg_rmt_file_path_join,200    #success
				except Exception as e:
					print(e)
					return self.myBucketName,'Not able to upload,Check connection',404 ##error in upload
			else:
				return self.myBucketName,'Check if file present to upload',403   ##local file not present


		if file_type == 'json':
			###Handle the correct remote path with file name
			rmt_file_path_split = remote_file_pathname.split('/')
			rmt_file_path_split[-1] = local_file_name.split('/')[1]
			rmt_file_path_join = '/'.join(rmt_file_path_split)

			#Upload file to S3 bucket
			if(os.path.exists(local_file_name)):
				try:
					self.bucket.put_object_from_file(rmt_file_path_join, local_file_name)
					print("File:'",local_file_name,"' Uploaded successfully to path:",rmt_file_path_join)
					return self.myBucketName,rmt_file_path_join,200    #success
				except Exception as e:
					print(e)
					return self.myBucketName,'Not able to upload,Check connection',404    ##error in upload
			else:
				return self.myBucketName,'Check if file present to upload',403   ##local file not present


if __name__ == '__main__':
	myBucketName = 'cwd-nexus-dev'
	remote_file_pathname = "documents/member/7321/id_document/ktp.jpg"
	s3 = s3_conn()

	local_file,status = s3.down_frm_s3(remote_file_pathname)
	#local_file_name =
	#remote_url,status = upl_to_s3(myBucketName,local_file_name,remote_file_pathname,file_type = 'image')

	#os.path.basename(local_file)
