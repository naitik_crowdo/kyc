# -*- coding: utf-8 -*-
"""
Created on Mon Aug 20 14:41:52 2018

@author: CWDSG05
"""
import boto3
from botocore.exceptions import ClientError
import os

class s3_conn(object):

	def __init__(self):
		self.S3_OBJECT = boto3.client(
		's3',
		region_name="ap-southeast-1",
		aws_access_key_id="AKIAJTBOSBOZLNR2GE2A",
		aws_secret_access_key= "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"
		)
		self.myBucketName = 'cwd-nexus'
		
		self.S3_OBJECT_staging = boto3.client(
		's3',
		region_name="ap-southeast-1",
		aws_access_key_id="AKIAIXCBUXYU6DTBUMVQ",
		aws_secret_access_key= "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"
		)
		self.myBucketName_staging = 'cwd-nexus-dev'

	def down_frm_s3(self,remote_file_pathname):
		local_file_name = 'files/'+os.path.basename(remote_file_pathname).split('.')[0]+'_cp.'+os.path.basename(remote_file_pathname).split('.')[1]
		try:
			self.S3_OBJECT.download_file(self.myBucketName,remote_file_pathname, local_file_name)
			print('File Downloaded successfully:',local_file_name)
			return local_file_name,200
		except ClientError as e:
			if e.response['Error']['Code'] == "404":
				print("The object does not exist.")
				return None,404
			else:
				return None,403


	def upl_to_s3(self,myBucketName,local_file_name,remote_file_pathname,file_type='image'):
		'''#remote file pathname will be initial key path received from API and it will be managed based on file_type to upload automatically
	'''
		if file_type == 'image':  #it means it is from transformation script and needs to be uploaded on new_folder with initial filename altered
			'''#Work on changing the path of Upload
	'''
			rmt_file_path_split = remote_file_pathname.split('/')
			chg_rmt_file_path_split = [split if split!='documents' else 'ocr' for split in rmt_file_path_split]
			#print('split after:',chg_rmt_file_path_split)
			'''#work on changing the name of upload
	'''
			chg_rmt_file_path_split_nmChange = chg_rmt_file_path_split[-1].split('.')[0]+'_transform.jpg'  #Add _cut in last of image for upload
			chg_rmt_file_path_split[-1] = chg_rmt_file_path_split_nmChange
			chg_rmt_file_path_join = '/'.join(chg_rmt_file_path_split)
			#print('after name change',chg_rmt_file_path_join)

			##Change the name of orginal name to transform name
			#os.path.basename(local_file_name).split('.')[0] = os.path.basename(local_file_name).split('.')[0]+'_transform.jpg'

			#Upload file to S3 bucket
			if(os.path.exists(local_file_name)):
				try:
					self.S3_OBJECT.upload_file(local_file_name, self.myBucketName, chg_rmt_file_path_join)
					print("File:'",local_file_name,"' Uploaded successfully to path:",chg_rmt_file_path_join)
					return self.myBucketName,chg_rmt_file_path_join,200    #success
				except ClientError as e:
					#print("error in uploading")
					return self.myBucketName,'Not able to upload,Check connection',404    ##error in upload
			else:
				return self.myBucketName,'Check if file present to upload',403   ##local file not present

		if file_type == 'json':
			###Handle the correct remote path with file name
			rmt_file_path_split = remote_file_pathname.split('/')
			rmt_file_path_split[-1] = local_file_name.split('/')[1]
			rmt_file_path_join = '/'.join(rmt_file_path_split)

			#Upload file to S3 bucket
			if(os.path.exists(local_file_name)):
				try:
					self.S3_OBJECT.upload_file(local_file_name, self.myBucketName, rmt_file_path_join)
					print("File:'",local_file_name,"' Uploaded successfully to path:",rmt_file_path_join)
					return self.myBucketName,rmt_file_path_join,200    #success
				except ClientError as e:
					#print("error in uploading")
					return self.myBucketName,'Not able to upload,Check connection',404    ##error in upload
			else:
				return self.myBucketName,'Check if file present to upload',403   ##local file not present


if __name__ == '__main__':
	myBucketName = 'cwd-nexus-dev'
	remote_file_pathname = "documents/member/7321/id_document/ktp.jpg"
	s3 = s3_conn()

	local_file,status = s3.down_frm_s3(remote_file_pathname)
	#local_file_name =
	#remote_url,status = upl_to_s3(myBucketName,local_file_name,remote_file_pathname,file_type = 'image')

	#os.path.basename(local_file)
