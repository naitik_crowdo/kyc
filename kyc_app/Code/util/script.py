from util.point_transform import four_point_transform
#import point_transform as f1
import imutils

def transform(image,refPt,angle=0,ratio=1):
	#box = np.int0(refPt)
	image_transform = four_point_transform(image, refPt.reshape(4, 2)*ratio)

	if angle != 0:	#first it will check for angle if present rotate the image
		image_transform = imutils.rotate_bound(image_transform, angle)

	return image_transform

if __name__=="__main__":
	import cv2,os

	os.chdir(r"C:\naitik\ocr_part1_and_part2\ktp+pass")

	# The name of the image file to extract text
	file_name = 'files/medium_KTP_Test_transform_cp.jpg'

	image = cv2.imread(file_name)
	rot_image = imutils.rotate_bound(image, 355)

	cv2.imshow('ImageWindow', rot_image)
	cv2.waitKey(0)
	cv2.destroyAllWindows()
