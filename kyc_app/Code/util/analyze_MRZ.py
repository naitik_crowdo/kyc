# -*- coding: utf-8 -*-
"""
Created on Thu Jun 14 18:21:29 2018

@author: Naitik
"""
from collections import OrderedDict   #from library
from datetime import datetime

class MRZ_p(object):

    __provides__ = ['MRZ_processed']
    __depends__ = ['MRZ_text']
    def __init__(self, mrz_lines):
        """
        Parse a TD1/TD2/TD3/MRVA/MRVB MRZ from a single newline-separated string or a list of strings.
        :param mrz_lines: either a single string with newlines, or a list of 2 or 3 strings, representing the lines of an MRZ.
        :return: self
        """
        self._parse(mrz_lines)
        self.aux = {}


    @staticmethod
    def from_ocr(mrz_ocr_string):
        """Given a single string which is output from an OCR routine, cleans it up using MRZ.ocr_cleanup and creates a MRZ object"""
        return MRZ_p(MRZOCRCleaner.apply(mrz_ocr_string))

    def __repr__(self):
        if self.valid:
            return 'VALID'
            #print("MRZ(TYPE[valid], ID number, FirstName, surname, Gender, BirthDate, IdExpiryDate)")
            #return "MRZ({0}[valid], {1}, {2}, {3}, {4}, {5}, {6})".format(self.mrz_type, self.number, self.names, self.surname, self.sex, self.date_of_birth,self.expiration_date)
        elif self.valid_score > 0:
            return 'SCORED'
            #print("MRZ(TYPE[Score], ID number, FirstName, surname, Gender, BirthDate, IdExpiryDate)")
            #return "Result({0}[{1}], {2}, {3}, {4}, {5}, {6}, {7})".format(self.mrz_type, self.valid_score, self.number, self.names, self.surname, self.sex, self.date_of_birth,self.expiration_date)
        else:
            return 'INVALID'


    def _parse(self, mrz_lines):
        self.mrz_type = MRZOCRCleaner._guess_type(mrz_lines)
        try:
            '''Need to write other parsing functions also for other passport format,
						Untill then it won't fails as Exception gonna raise and return INVALID response'''
            if self.mrz_type == 'TD1':
                self.valid = self._parse_td1(*mrz_lines)
            elif self.mrz_type == 'TD2':
                self.valid = self._parse_td2(*mrz_lines)
            elif self.mrz_type == 'TD3':
                self.valid = self._parse_td3(*mrz_lines)
            elif self.mrz_type == 'MRVA':
                self.valid = self._parse_mrv(*mrz_lines, length=44)
            elif self.mrz_type == 'MRVB':
                self.valid = self._parse_mrv(*mrz_lines, length=36)
            else:
                self.valid = False
                self.valid_score = 0
        except Exception:
            self.mrz_type = None
            self.valid = False
            self.valid_score = 0

    def to_dict(self):
        """Converts this object to an (ordered) dictionary of field-value pairs.
        >>> m = MRZ(['IDAUT10000999<6<<<<<<<<<<<<<<<', '7109094F1112315AUT<<<<<<<<<<<6', 'MUSTERFRAU<<ISOLDE<<<<<<<<<<<<']).to_dict()
        >>> assert m['type'] == 'ID' and m['country'] == 'AUT' and m['number'] == '10000999<'
        >>> assert m['valid_number'] and m['valid_date_of_birth'] and m['valid_expiration_date'] and not m['valid_composite']
        """

        result = OrderedDict()
        result['mrz_type'] = self.mrz_type
        result['valid_score'] = self.valid_score
        if self.mrz_type is not None:
            result['type'] = self.type
            result['country'] = self.country
            result['number'] = self.number
            result['date_of_birth'] = self.date_of_birth
            result['expiration_date'] = self.expiration_date
            result['nationality'] = self.nationality
            result['sex'] = self.sex
            result['names'] = self.names
            result['surname'] = self.surname
            if self.mrz_type == 'TD1':
                result['optional1'] = self.optional1
                result['optional2'] = self.optional2
            elif self.mrz_type in ['TD2', 'MRVA', 'MRVB']:
                result['optional1'] = self.optional1
            else:
                result['personal_number'] = self.personal_number
            result['check_number'] = self.check_number
            result['check_date_of_birth'] = self.check_date_of_birth
            result['check_expiration_date'] = self.check_expiration_date
            if self.mrz_type not in ['MRVA', 'MRVB']:
                result['check_composite'] = self.check_composite
            if self.mrz_type == 'TD3':
                result['check_personal_number'] = self.check_personal_number
            result['valid_number'] = self.valid_check_digits[0]
            result['valid_date_of_birth'] = self.valid_check_digits[1]
            result['valid_expiration_date'] = self.valid_check_digits[2]
            if self.mrz_type not in ['MRVA', 'MRVB']:
                result['valid_composite'] = self.valid_check_digits[3]
            if self.mrz_type == 'TD3':
                result['valid_personal_number'] = self.valid_check_digits[4]
            if 'method' in self.aux:
                result['method'] = self.aux['method']
        return result

    def _parse_td3(self, a, b):
        len_a, len_b = len(a), len(b)
        if len(a) < 44:
            a = a + '<'*(44 - len(a))
        if len(b) < 44:
            b = b + '<'*(44 - len(b))
        self.type = a[0:2]
        self.country = a[2:5]
        surname_names = a[5:44].split('<<', 1)
        if len(surname_names) < 2:
            surname_names += ['']
        self.surname, self.names = surname_names
        self.names = self.names.replace('<', ' ').strip()
        self.surname = self.surname.replace('<', ' ').strip()
        self.number = b[0:9].replace('<','').strip()
        self.check_number = b[9]
        self.nationality = b[10:13]
        self.date_of_birth = b[13:19]
        self.check_date_of_birth = b[19]
        '''Code to regularize Sex letter irregularities'''
        #self.sex = b[20] if b[20] in ['F','M'] else 'UNK'
        if b[20] in ['H','M','4','A','8','N']: self.sex = 'MALE'
        elif b[20] in ['F','P','9','E']: self.sex ='FEMALE'
        else: self.sex= b[20]
        '''Code to regularize date error'''
        self.expiration_date = b[21:27]
        self.check_expiration_date = b[27]
        #self.personal_number = b[28:42]
        if b[28] in ['5','8','7','6']:
            if b[28] in ['5','6','8']: self.personal_number = 'S'+b[29:42]
            elif b[28] in ['7']: self.personal_number = 'T'+b[29:42]
        else: self.personal_number = b[28:42]
        self.check_personal_number = b[42]
        self.check_composite = b[43]
        self.valid_check_digits = [MRZCheckDigit.compute(self.number) == self.check_number,
                             MRZCheckDigit.compute(self.date_of_birth) == self.check_date_of_birth and MRZ_p._check_date(self.date_of_birth),
                             MRZCheckDigit.compute(self.expiration_date) == self.check_expiration_date and MRZ_p._check_date(self.expiration_date),
                             MRZCheckDigit.compute(b[0:10] + b[13:20] + b[21:43]) == self.check_composite,
                             ((self.check_personal_number == '<' or self.check_personal_number == '0') and self.personal_number == '<<<<<<<<<<<<<<') # PN is optional
                                or MRZCheckDigit.compute(self.personal_number) == self.check_personal_number]
        self.valid_line_lengths = [len_a == 44, len_b == 44]
        self.valid_misc = [a[0] in 'P']
        self.valid_score = 10*sum(self.valid_check_digits) + sum(self.valid_line_lengths) + sum(self.valid_misc) +1
        self.valid_score = 100*self.valid_score//(50+2+1+1)
        self.valid_number, self.valid_date_of_birth, self.valid_expiration_date, self.valid_personal_number, self.valid_composite = self.valid_check_digits
        return self.valid_score == 100

    @staticmethod
    def _check_date(ymd):
        try:
            datetime.strptime(ymd, '%y%m%d')
            return True
        except ValueError:
            return False


class MRZOCRCleaner(object):
    """
    The __call__ method of this class implements the "cleaning" of an OCR-obtained string in preparation for MRZ parsing.
    This is a singleton class, so rather than creating an instance, simply use its `apply` static method.
    >>> MRZOCRCleaner.apply('\\nuseless lines\\n  P<POLKOWALSKA < KWIATKOWSKA<<JOANNA<<<<<<<<<<<extrachars \\n  AA0000000OP0L6OOzoB4Fi4iz3I4<<<<<<<<<<<<<<<4  \\n  asdf  ')
    ['P<POLKOWALSKA<KWIATKOWSKA<<JOANNA<<<<<<<<<<<extrachars', 'AA00000000POL6002084F1412314<<<<<<<<<<<<<<<4']
    """

    def __init__(self):
        # Specifications for which characters may be present at each position of each line of each document type.
        #   a  - alpha
        #   A  - alpha+<
        #   n  - numeric
        #   N  - numeric+<
        #   *  - alpha+num+<

        TD1 = ['a*' + 'A'*3 + '*'*9 + 'N' + '*'*15,
               'n'*7 + 'A' + 'n'*7 + 'A'*3 + '*'*11 + 'n',
               'A'*30]
        TD2 = ['a' + 'A'*35,
               '*'*9 + 'n' + 'A'*3 + 'n'*7 + 'A' + 'n'*7 + '*'*7 + 'n'*1]
        TD3 = ['a' + 'A'*43,
               '*'*9 + 'n' + 'A'*3 + 'n'*7 + 'A' + 'n'*7 + '*'*14 + 'n'*2 ]
        MRV = ['a' + 'A'*43,
               '*'*9 + 'n' + 'A'*3 + 'n'*7 + 'A' + 'n'*7 + '*'*16 ]
        self.FORMAT = {'TD1': TD1, 'TD2': TD2, 'TD3': TD3, 'MRVA': MRV, 'MRVB': MRV}

        # Fixers
        a = {'0': 'O', '1': 'I', '2': 'Z', '4': 'A', '5': 'S', '6': 'G', '8': 'B' }
        n = {'B': '8', 'C': '0', 'D': '0', 'G': '6', 'I': '1', 'O': '0', 'Q': '0', 'S': '5', 'Z': '2', 'U':'0'}
        self.FIXERS = {'a': a, 'A': a, 'n': n, 'N': n, '*': {}}

    def _split_lines(self, mrz_ocr_string):
        return [ln for ln in mrz_ocr_string.replace(' ', '').split('\n') if (len(ln) >= 20 or '<<' in ln)]

    def __call__(self, mrz_ocr_string):
        """
        Given a string, which is output from an OCR routine, splits it into lines and performs various ad-hoc cleaning on those.
        In particular:
            - Spaces are removed
            - Lines shorter than 30 non-space characters are removed
            - The type of the document is guessed based on the number of lines and their lengths,
              if it is not-none, OCR-fixup is performed on a character-by-character basis depending on
              what characters are allowed at particular positions.
        """
        lines = self._split_lines(mrz_ocr_string)
        tp = self._guess_type(lines)
        if tp is not None:
            for i in range(len(lines)):
                lines[i] = self._fix_line(lines[i], tp, i)
        return lines

    def _fix_line(self, line, type, line_idx):
        ln = list(line)
        for j in range(len(ln)):
            ln[j] = self._fix_char(ln[j], type, line_idx, j)
        return ''.join(ln)

    def _fix_char(self, char, type, line_idx, char_idx):
        fmt = self.FORMAT[type][line_idx]
        if char_idx >= len(fmt):
            return char
        else:
            fixer = self.FIXERS[fmt[char_idx]]
            char = char.upper()
            return fixer.get(char, char)

    @staticmethod
    def apply(txt):
        if getattr(MRZOCRCleaner, '__instance__', None) is None:
            MRZOCRCleaner.__instance__ = MRZOCRCleaner()
        return MRZOCRCleaner.__instance__(txt)

    @staticmethod
    def _guess_type(mrz_lines):
        """Guesses the type of the MRZ from given lines. Returns 'TD1', 'TD2', 'TD3', 'MRVA', 'MRVB' or None.
        The algorithm is basically just counting lines, looking at their length and checking whether the first character is a 'V'
        """
        try:
            if len(mrz_lines) == 3:
                return 'TD1'
            elif len(mrz_lines) == 2 and len(mrz_lines[0]) < 40 and len(mrz_lines[1]) < 40:
                return 'MRVB' if mrz_lines[0][0].upper() == 'V' else 'TD2'
            elif len(mrz_lines) == 2:
                return 'MRVA' if mrz_lines[0][0].upper() == 'V' else 'TD3'
            else:
                return None
        except:
            return None


class MRZCheckDigit(object):
    """
    The algorithm used to compute "check digits" within MRZ.
    Its __call__ method, given a string, returns either the single character check digit.
    Rather than creating an instance every time, use the static compute(txt) method (which makes use of a singleton instance).
    # Valid codes
    >>> assert MRZCheckDigit.compute('0') == '0'
    >>> assert MRZCheckDigit.compute('0000000000') == '0'
    >>> assert MRZCheckDigit.compute('00A0A<0A0<<0A0A0<0A') == '0'
    >>> assert MRZCheckDigit.compute('111111111') == '3'
    >>> assert MRZCheckDigit.compute('111<<<111111') == '3'
    >>> assert MRZCheckDigit.compute('BBB<<<1B1<<<BB1') == '3'
    >>> assert MRZCheckDigit.compute('1<<1<<1<<1') == '8'
    >>> assert MRZCheckDigit.compute('1<<1<<1<<1') == '8'
    >>> assert MRZCheckDigit.compute('BCDEFGHIJ') == MRZCheckDigit.compute('123456789')
    # Invalid codes
    >>> assert MRZCheckDigit.compute('') == ''
    >>> assert MRZCheckDigit.compute('0000 0') == ''
    >>> assert MRZCheckDigit.compute('0 0') == ''
    >>> assert MRZCheckDigit.compute('onlylowercase') == ''
    >>> assert MRZCheckDigit.compute('BBb<<<1B1<<<BB1') == ''
    """

    def __init__(self):
        self.CHECK_CODES = dict()
        for i in range(10):
            self.CHECK_CODES[str(i)] = i     #from 0 to 9 in dict
        for i in range(ord('A'), ord('Z')+1):
            self.CHECK_CODES[chr(i)] = i - 55   # A --> 10, B --> 11, etc
        self.CHECK_CODES['<'] = 0
        self.CHECK_WEIGHTS = [7, 3, 1]

    def __call__(self, txt):
        if txt == '':
            return ''
        res = sum([self.CHECK_CODES.get(c, -1000)*self.CHECK_WEIGHTS[i % 3] for i, c in enumerate(txt)])
        if res < 0:
            return ''
        else:
            return str(res % 10)

    @staticmethod
    def compute(txt):
        if getattr(MRZCheckDigit, '__instance__', None) is None:
            MRZCheckDigit.__instance__ = MRZCheckDigit()
        return MRZCheckDigit.__instance__(txt)


