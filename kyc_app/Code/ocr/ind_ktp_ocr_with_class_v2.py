# -*- coding: utf-8 -*-
""" 
Created on Thu Jul 12 15:03:29 2018

@author: CWDSG05
Updated on 10-09 for update in logic of unordered tags returned by vision (currently Name only comes first and identified as 0 tag, so in vision group grouping first corrected to take dynamic buffer and
 understand unordered tags coming from vision by just looking iteratively for all y axis).
"""

from ocr.ocr_utils import _most_common,_process_gender_and_dob_from_NIK_dob,_correct_NIK,_extract_from_NIK
from pandas import read_csv
from pprint import pprint
from difflib import get_close_matches

class process_ocr(object):

	def __init__(self):
		self.PROVINCE = 99
		self.DISTRICT = 99
		self.NIK = 99
		self.NAME = 99
		self.BIRTHPLACE = 99
		self.GENDER = 99
		self.ADDRESS1 = 99
		self.ADDRESS2 = 99
		self.ADDRESS3 = 99
		self.ADDRESS4 = 99
		self.RELIGION = 99
		self.MARRIED = 99
		self.EMPLOYMENT = 99
		self.FINANCE = 99

		self.married_ext = None
		self.religion_ext= None

		self.ORDER = \
			{
			 1: ['PROVINSI'],
			 2: ['DISTRICT'],
			 3: ['NIK'],
			 4: ['NAMA','NAM'],
			 5: ['TEMPAT','TGL','LAHIR','TGI'],
			 6: ['JENIS','KELAMIN'],
			 7: ['ALAMAT'],
			 8: ['RTRW','RT','RW'],
			 9: ['KELDESA','KEVDESA','DESA','KE','KEI'],
			 10: ['KECAMATAN'],
			 11: ['AGAMA'],
			 12: ['STATUS','PERKAWINAN'],
			 13: ['PEKERJAAN'],
			 14: ['KEWARGANEGARAAN']
			 }
		self.ORDER_WORDS = \
			{1: 'PROVINCE',
			 2: 'DISTRICT',
			 3: 'NIK',
			 4: 'NAME',
			 5: 'BIRTHPLACE',
			 6: 'GENDER',
			 7: 'ADDRESS1',
			 8: 'ADDRESS2',
			 9: 'ADDRESS3',
			10: 'ADDRESS4',
			11: 'RELIGION',
			12: 'MARRIED',
			13: 'EMPLOYMENT',
			14: 'FINANCE'
			 }
		self.religion = ['KATHOLIK','KRISTEN','ISLAM',"BUDHA",'HINDU','PROTESTANT','KHONGHUCU']
		self.nik_fixer = {'B': '8', 'C': '0', 'D': '0', 'G': '6', 'I': '1', 'O': '0', 'Q': '0', 'S': '5', 'Z': '2', 'U':'0'}

	def __call__(self):
		#read region data
		#region_data_filename = r'C:\Users\CWDSG05\Desktop\KYC\indonesia_region_data.csv'
		region_data_filename = 'master/indonesia_region_data.csv'
		self.region_data = read_csv(region_data_filename,header=0)
		self.region_data.set_index('code', inplace=True)

	@staticmethod
	def find_key_from_value(dict1,search):
		for k,v in dict1.items():
			if v==search:
				return k
		return 99

	def process_religion(self,inp_dict1):
		'''
		find religion key in our order(11 for now), and search this key present in incoming dictionary,
		If key is present try to find the Religion words mention above and update the value in incoming dictioanry to that,
		If not then just update null list entry in place.
		'''
		inp_dict = inp_dict1.copy()
		religion_key = self.find_key_from_value(self.ORDER_WORDS,'RELIGION')
		if religion_key != 99:
			try:
				religion_list = inp_dict[religion_key]
			except:
				religion_list = []
			if len(religion_list)!=0:
				for words in self.religion:
					if words in religion_list:
						self.religion_ext = words
						inp_dict[religion_key] = [words]
			else:
				inp_dict[religion_key] = []
		#print("Religion: ",self.religion_ext)
		return inp_dict

	def process_maritial_status(self,inp_dict1):
		inp_dict = inp_dict1.copy()
		single_word_check = ['BELUM','8ELUM','BEL0M','8EL0M','BELOM','BELOV']
		married_word_check = ['KAWIN','KAW1N']
		if self.MARRIED != 99:
			marriage_key = self.find_key_from_value(self.ORDER_WORDS,'MARRIED')
			try:
				married_list = inp_dict[marriage_key]
			except:
				married_list = []
			if len(married_list) != 0:
				for words in married_list:
					if words in married_word_check and self.married_ext==None:
						self.married_ext = 'MARRIED'
						#print(self.married_ext)
						inp_dict[marriage_key] = ['MARRIED']
					if words in single_word_check:
						self.married_ext = 'SINGLE'
						#print(self.married_ext)
						inp_dict[marriage_key] = ['SINGLE']
			else:
				inp_dict[marriage_key] = []
		#print("marriage status: ",self.married_ext)
		return inp_dict


	def capitalize_dict(self,in_dict1):
		in_dict = in_dict1.copy()
		##Capitalize the input dictionary with list as values
		temp_dict = {}
		for k,v in in_dict.items():
			temp_list = []
			for word in v:
				temp_list.append(word.upper())
			temp_dict[k] = temp_list
		return temp_dict

	#def map_literals_from_text(self,text_after_group_cp):
		#ORDER_FOUND_LINES={}
		#limit = len(text_after_group_cp)-1
		#last_match = 0
		#for k,order_list in self.ORDER.items():
		#	row_belongs = None
		#	match_here = []
		#	to_check = []
		#	#Create a list of lines to be compared against ORDER with text_after_group
		#	if k==0:
		#		to_check = [0,1,2]
		#	elif k==limit:
		#		to_check = list(range(last_match,limit))
		#	else:
		#		to_check = list(range(last_match,k+1))

		#	for check_word in order_list:
		#		for i in to_check:
		#			if check_word in text_after_group_cp[i]:
		#				match_here.append(i)
		#	if len(match_here) != 0:
		#		row_belongs = _most_common(match_here)  #find most common entry from matched literals
		#		ORDER_FOUND_LINES[k] = text_after_group_cp[row_belongs] #insert into new dictionary their order_index with values
		#		#print("assigning variables")
		#		exec('self.'+"%s = %d" % (self.ORDER_WORDS[k], row_belongs))      #assign corresponding variables to line numbers from received dict
		#return ORDER_FOUND_LINES


	def map_literals_from_text(self,text_after_group_cp,debug=False):
		''' 
		Updated to map literals known to the text received, set debug=True to see which  Tag match with which lines from raw text, after this _most_common method will be called which will
		extract the line number with most repeated tags found.
		'''
		ORDER_FOUND_LINES={}
		for i,order_tag_list in self.ORDER.items():
			#for all the known tags that we can search
			match_here = []
			for order_tag_words in order_tag_list:
				#for all words inside the list
				for k,raw_text_list in text_after_group_cp.items():
					if order_tag_words in raw_text_list:
						match_here.append(k) #add the line for match word found in raw text list
						#ORDER_FOUND_LINES[i] = text_after_group_cp[k]
						#break
			if len(match_here) != 0: #it means some line is identified
				if debug: print("searching for: '{}' match found in lines:{} in text_aftr_group".format(self.ORDER_WORDS[i],match_here))
				row_belongs = _most_common(match_here) #find most common entry from matched dictionary line
				ORDER_FOUND_LINES[i] = text_after_group_cp[row_belongs] #insert into new dictionary their order_index with values
				#print("assigning variables")
				exec('self.'+"%s = %d" % (self.ORDER_WORDS[i], row_belongs))      #assign corresponding variables to line numbers from received dict
			#del text_after_group_cp[k]
		return ORDER_FOUND_LINES


	def handle_district(self,map_text_after_group_cp,text_after_group_cp):
		'''tries to handle missing district if Province and NIK are identified'''
		if self.DISTRICT == 99 and self.PROVINCE!=99 and self.NIK == self.PROVINCE+2:
			self.DISTRICT= self.PROVINCE+1
			map_text_after_group_cp[2]= text_after_group_cp[self.DISTRICT] #2 is th position in ORDER_WORDS dict for District
		return map_text_after_group_cp


	def remove_matched_words_from_map_dict(self,ORDER_FOUND_LINES):
		'''Tries to do cleansing of words from ORDER dictionary matched words'''
		temp_dict={}
		for k,v in ORDER_FOUND_LINES.items():
			temp_list = []
			for words_to_check in v:
				if words_to_check not in self.ORDER[k]:
					temp_list.append(words_to_check)
			temp_dict[k] = temp_list
		return temp_dict


	def process_NIK(self,nik_str,inp_dict1):
		'''Check for 16 digit length and contains only digit
		check for nik_var value to debug:
			if value =99:   Length is not correct
			if vlalue > 50:    Contains non-digit letters
			any variable value = 99 :   Not able to extract
		'''
		inp_dict = inp_dict1.copy()
		for nik in nik_str:
			#check for length first
			if len(nik) != 16:
				self.NIK = 99
				self.province_ext,self.district_ext,self.subdistrict_ext,self.gender_ext,self.dob_ext = 99,99,99,99,99
			else:
				nik_str_corr = _correct_NIK(nik,self.nik_fixer) ##Try to fix the content to digits before checking
				if not nik_str_corr.isdigit():
					self.NIK = 50+self.NIK
					if nik_str_corr[6:12].isdigit():
						var_gender_ext,var_dob_ext = _process_gender_and_dob_from_NIK_dob(nik_str_corr[6:12])
						self.gender_ext, self.dob_ext = 21,22 #found values for these 2 variables so giving number
						'''update founded values with their code on self dictionary'''
						self.ORDER_WORDS[self.gender_ext] = [var_gender_ext]
						self.ORDER_WORDS[self.dob_ext] = [var_dob_ext]
						#update the values
						inp_dict.update({self.gender_ext:var_gender_ext, self.dob_ext:var_dob_ext})
					#else:
						#province_ext,district_ext,subdistrict_ext,gender_ext,dob_ext = 99,99,99,99,99
				else:
					var_province_ext,var_district_ext,var_subdistrict_ext,var_gender_ext,var_dob_ext = _extract_from_NIK(nik_str_corr,self.region_data)
					self.gender_ext, self.dob_ext,self.province_ext,self.district_ext,self.subdistrict_ext = 21,22,23,24,25
					'''update into ORDER_WORDS dict founded codes'''
					self.ORDER_WORDS.update({self.gender_ext:'GENDER_EXT',self.dob_ext:'DOB_EXT', self.province_ext:"PROVINCE_EXT", self.district_ext: 'DISTRICT_EXT', self.subdistrict_ext:'SUBDISTRICT_EXT'})
					'''update the value with relevant code into dictionary to return'''
					inp_dict.update({self.gender_ext:[var_gender_ext], self.dob_ext:[var_dob_ext], self.province_ext:[var_province_ext], self.district_ext: [var_district_ext], self.subdistrict_ext:[var_subdistrict_ext]})
		#print(var_province_ext,var_district_ext,var_subdistrict_ext,var_gender_ext,var_dob_ext)
		return inp_dict
		#return province_ext,district_ext,subdistrict_ext,gender_ext,dob_ext



	def print_var(self):
		#print("Province: {}\nDistrict: {}\nNIK: {}\nName: {}\n".format(self.PROVINCE,self.DISTRICT,self.NIK,self.NAME))
		#print(self.region_data)
		pprint(self.ORDER_WORDS)
		#pprint(self.ORDER)

	@staticmethod
	def is_number(s):
	    try:
	        float(s)
	        return True
	    except ValueError:
	        pass
	    try:
	        import unicodedata
	        unicodedata.numeric(s)
	        return True
	    except (TypeError, ValueError):
	        pass
	    return False

	def get_results(self,in_dict1):
		in_dict = in_dict1.copy()
		temp_dict={}
		for k,v in in_dict.items():
			try:
				tag = self.ORDER_WORDS[k]
				temp_dict[tag] = ' '.join(v)
			except:
				pass
		return temp_dict

	def process_dob_in_dict(self,in_dict1):
		in_dict = in_dict1.copy()
		birth_key = self.find_key_from_value(self.ORDER_WORDS,'BIRTHPLACE')
		#print('birth key',birth_key)
		date_list = []
		place_list = []
		try:
			extract_list = in_dict[birth_key]
		except:
			return in_dict
		for word in extract_list:
			numb_fix_word = _correct_NIK(word,self.nik_fixer)  #correct if any wrong number mis read
			if self.is_number(numb_fix_word):
				date_list.append(numb_fix_word)
			elif numb_fix_word in ['-','.',',',':']: pass
			else: place_list.append(word)
		if len(place_list) != 0:
			in_dict[birth_key] = place_list
		if len(date_list) != 0:
			in_dict[20] = ['-'.join(date_list)]
			self.ORDER_WORDS[20] = 'DOB'
		#pprint("Order_words: ",self.ORDER_WORDS)
		return in_dict

	def extract_gender_from_dict(self,inp_dict1):
		inp_dict = inp_dict1.copy()
		try:
			gender_list = inp_dict[self.find_key_from_value(self.ORDER_WORDS,'GENDER')]
			match = get_close_matches('PEREMPUAN', gender_list, 1, 0.70)
			if match:
				inp_dict[self.find_key_from_value(self.ORDER_WORDS,'GENDER')] = ['FEMALE']
			else:
				inp_dict[self.find_key_from_value(self.ORDER_WORDS,'GENDER')] = ['MALE']
		except:
			inp_dict[self.find_key_from_value(self.ORDER_WORDS,'GENDER')] = ['UNKNOWN']

		return inp_dict

	def score_check(self,inp_dict1):
		self.score =0
		inp_dict = inp_dict1.copy()
		'''check for DOB'''
		try:
			if get_close_matches(inp_dict['DOB'],[inp_dict['DOB_EXT']], 1, 0.80):
				self.score += 1
		except:
			pass
		'''check for PROVINCE'''
		try:
			if get_close_matches(inp_dict['PROVINCE'],[inp_dict['PROVINCE_EXT']], 1, 0.80):
				self.score += 1
		except:
			pass
		'''check for DISTRICT'''
		try:
			if get_close_matches(inp_dict['DISTRICT'],[inp_dict['DISTRICT_EXT']], 1, 0.50):
				self.score += 1
		except:
			pass
		try:
			if inp_dict['NAME']:
				self.score += 1
		except:
			pass
		try:
			if len(inp_dict['NIK'])==16:
				self.score += 1
		except:
			pass
		return self.score

	def return_dict(self,inp_dict):
		return_dict={}
		#for Name
		try:
			return_dict['NAME']= inp_dict['NAME']
		except:
			return_dict['NAME']= None
		#for NIK
		try:
			return_dict['NIK']= inp_dict['NIK']
		except:
			return_dict['NIK']= None
		#For province
		try:
			return_dict['PROVINCE']= inp_dict['PROVINCE_EXT']
		except:
			try:
				return_dict['PROVINCE']= inp_dict['PROVINCE']
			except:
				return_dict['PROVINCE']= None
		#for District
		try:
			return_dict['DISTRICT']= inp_dict['DISTRICT_EXT']
		except:
			try:
				return_dict['DISTRICT']= inp_dict['DISTRICT']
			except:
				return_dict['DISTRICT']= None
		#for DOB
		try:
			return_dict['DOB']= inp_dict['DOB_EXT']
		except:
			try:
				return_dict['DOB']= inp_dict['DOB']
			except:
				return_dict['DOB']= None
		#for Gender
		try:
			return_dict['GENDER']= inp_dict['GENDER_EXT']
		except:
			try:
				return_dict['GENDER']= inp_dict['GENDER']
			except:
				return_dict['GENDER']= None
		#for maritial status
		try:
			return_dict['STATUS']= inp_dict['MARRIED']
		except:
			return_dict['STATUS']= None
		#for Address
		address_find = []
		for key, value in inp_dict.items():   # iter on both keys and values
			if key.startswith('ADDRESS'):
				address_find.append(value)
		try:
			return_dict['ADDRESS']= ','.join(address_find)
		except:
			return_dict['ADDRESS']= None
		#for score
		try:
			return_dict['SCORE']= self.score
		except:
			return_dict['SCORE']= None

		return return_dict

class call_ocr(object):
	def __init__(self):
		self.p = process_ocr() #Call __init__():
		self.p() #call __call__():

	def call_f_ocr(self,inp_dict1):
		text_after_group = inp_dict1.copy()
		#p = process_ocr() #Call __init__():
		#p() #call __call__():
		'''to make uppercase elements of dictionary'''
		text_after_group_cp = self.p.capitalize_dict(text_after_group)
		'''map the words from ORDER dict to compare from text_after_group_cp and take max matching words'''
		map_text_after_group_cp = self.p.map_literals_from_text(text_after_group_cp, debug = False) #here also mapping variables values


		'''try to see if PROVINSI is present and NIK is present and there's a gap of 1 in between, so that should be district'''
		dfix_map_text_after_group_cp = self.p.handle_district(map_text_after_group_cp, text_after_group_cp)
		#p.print_var()

		'''Filter the tags from the content with matched words'''
		filt_dfix_map_text_after_group_cp = self.p.remove_matched_words_from_map_dict(dfix_map_text_after_group_cp)

		'''fix the religion found from matching words'''
		rfix_filt_dfix_map_tt_ar_gp_cp = self.p.process_religion(filt_dfix_map_text_after_group_cp)

		'''fix the marriage status found from matched word (place 12)'''
		mfix_rfix_ft_dx_mp_tt_ar_gp_cp = self.p.process_maritial_status(rfix_filt_dfix_map_tt_ar_gp_cp)

		'''break the  place of birth and DOB if found in same line and create new entry'''
		break_place_nd_dob =  self.p.process_dob_in_dict(mfix_rfix_ft_dx_mp_tt_ar_gp_cp)

		try:
			NIK_str = break_place_nd_dob[3] # 3 is code in order for NIK
			if isinstance(NIK_str,list):
				#print('inside')
				break_place_nd_dob = self.p.process_NIK(NIK_str,break_place_nd_dob)
		except:
			pass

		ext_gender_brk_plc_nd_dob = self.p.extract_gender_from_dict(break_place_nd_dob)

		''''create the final dictionary where key are replaced by Tags again'''
		fin_result = self.p.get_results(ext_gender_brk_plc_nd_dob)
		score_returned = self.p.score_check(fin_result)

		fin = self.p.return_dict(fin_result)
		return fin


'''
text_after_group = {0: ['PROVINSI', 'DKI', 'JAKARTA'],
 1: ['JAKARTA', 'SELATAN'],
 2: ['3174106807950012', 'NIK'],
 3: ['Nama', 'DISTI', 'RAVENA', 'FAUZIAH'],
 4: ['Tempat', 'Tgl', 'Lahir', 'TANGERANG', ',', '28', '-', '07', '-', '1995'],
 5: ['Jenis', 'kelamin', 'PEREMPUAN', 'Gol', '.', 'Darah'],
 6: ['Alamat', 'L', 'KAMP', 'BINTARO'],
 7: ['RTRW', '2014', '/', '001'],
 8: ['KevDesa', 'PESANGGRAHAN'],
 9: ['Kecamatan', 'PESANGGRAHAN'],
 10: ['Agama', 'ISLAM', 'Status', 'Perkawinan', 'BELUM', 'KAWIN'],
 11: ['Pekerjaan', 'PELAJAR', '/', 'MAHASISWA'],
 12: ['Kewarganegaraan', 'WNI'],
 13: ['Berlaku', 'Hingga', 'SEUMUR', 'HIDUP'],
 14: ['JAKARTA', 'SELATAN']}

text_after_group = {0: ['PROVINSI', 'JAWA', 'TIMUR'],
 1: ['KOTA', 'PROBOLINGGO'],
 2: ['NIK', '3574035008900003'],
 3: ['Nama', 'RIZKI', 'FIRMANANDA'],
 4: ['Tempat', '/', 'Tgl', 'Lahir', 'PROBOLINGGO', ',', '10', '-', '08', '-', '1990'],
 5: ['Jenis', 'Kelamin', 'PEREMPUAN', 'Gol', 'Darah'],
 6: ['Alamat', 'JL', 'SERMA', 'ABD', 'RAHMAN'],
 7: ['RT', '/', 'RW', '002', '/', '004'],
 8: ['Ke', '\\', 'Desa', 'WIROBORANG'],
 9: ['Kecamatan', 'MAYANGAN'],
 10: ['Agama', 'ISLAM'],
 11: ['Status', 'Perkawinan', 'BELUM', 'KAWIN'],
 12: ['Pekerjaan', 'PELAJAR', '/', 'MAHASISWA'],
 13: ['Kewarganegaraan', 'WNI'],
 14: ['Berlaku', 'Hingga', '10', '-', '08', '-', '2017'],
 15: ['KOTA', 'PROBOLINGGO'],
 16: ['22', '-', '10', '-', '2012']}
 '''
if __name__=="__main__":
	p = process_ocr() #Call __init__():
	p() #call __call__():
	'''to make uuercase elements of dictionary'''
	text_after_group_cp = p.capitalize_dict(text_after_group)

	'''map the words from ORDER dict to compare from text_after_group_cp and take max matching words'''
	map_text_after_group_cp = p.map_literals_from_text(text_after_group_cp, debug = True) #here also mapping variables values


	'''try to see if PROVINSI is present and NIK is present and there's a gap of 1 in between, so that should be district'''
	dfix_map_text_after_group_cp = p.handle_district(map_text_after_group_cp,text_after_group_cp)
	#p.print_var()

	'''Filter the tags from the content with matched words'''
	filt_dfix_map_text_after_group_cp = p.remove_matched_words_from_map_dict(dfix_map_text_after_group_cp)

	'''fix the religion found from matching words'''
	rfix_filt_dfix_map_tt_ar_gp_cp = p.process_religion(filt_dfix_map_text_after_group_cp)

	'''fix the marriage status found from matched word (place 12)'''
	mfix_rfix_ft_dx_mp_tt_ar_gp_cp = p.process_maritial_status(rfix_filt_dfix_map_tt_ar_gp_cp)

	'''break the  place of birth and DOB if found in same line and create new entry'''
	break_place_nd_dob =  p.process_dob_in_dict(mfix_rfix_ft_dx_mp_tt_ar_gp_cp)

	try:
		NIK_str = break_place_nd_dob[3] # 3 is code in order for NIK
		if isinstance(NIK_str,list):
			#print('inside')
			break_place_nd_dob = p.process_NIK(NIK_str,break_place_nd_dob)
	except:
		pass

	ext_gender_brk_plc_nd_dob = p.extract_gender_from_dict(break_place_nd_dob)

	''''create the final dictionary where key are replaced by Tags again'''
	fin_result = p.get_results(ext_gender_brk_plc_nd_dob)
	score_returned = p.score_check(fin_result)

	pprint(fin_result)
	pprint(score_returned)
	#p.print_var()

	fin = p.return_dict(fin_result)
	pprint(fin)
#inp_dict = fin_result.copy()
'''
return_dict = {'NAME':None,'NIK':None,'PROVINCE':None,'DISTRICT':None,'DOB':None,'STATUS':None,
			   'GENDER':None,'ADDRESS':None}
'''



