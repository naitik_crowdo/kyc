# -*- coding: utf-8 -*-
"""
Created on Thu Jul 12 15:13:17 2018

@author: CWDSG05
"""
from datetime import datetime

def _most_common(lst):
    return max(set(lst), key=lst.count)

def _correct_NIK(nik_str,nik_fixer):
	'''Try to fix if any string digit came for number as per NIK_Fixer dict above'''
	cor_nik = []
	for letter in nik_str:
		if letter in nik_fixer:
			cor_nik.append(nik_fixer[letter])
		else:
			cor_nik.append(letter)
	return(''.join(cor_nik))

def _process_gender_and_dob_from_NIK_dob(dob_str):
	dd = int(dob_str[:2])
	mm = int(dob_str[2:4])
	yy = int(dob_str[4:])
	if dd > 40:
		gender_ext = 'FEMALE'
		dd = dd-40
	else:
		gender_ext = 'MALE'
	parse_date = datetime.strptime(str(dd)+str(mm)+str(yy), '%d%m%y')
	parse_date_format = datetime.strftime(parse_date,'%d-%m-%Y')
	return gender_ext,parse_date_format


def _extract_from_NIK(nik_str,region_data):
	'''
	Return province,district,gender and formatted dob extracted from NIK
	for any error or field not able to extract return code "99"
	'''
	region = int(nik_str[:6])
	dob = nik_str[6:12]
	#extract region information from code
	try:  #for province try block
		'''#extract the row matching with index region_cd and convert to dictionary'''
		temp_dict = region_data.loc[region].to_dict()
		province_ext,district_exr,subdistrict_ext  = temp_dict['province'],temp_dict['district'],temp_dict['sub-district']
	except:
		province_ext,district_exr,subdistrict_ext = 99,99,99
	try: # for dob and gender try block
		'''extract gender and formatted dob from string'''
		gender_ext,dob_ext = _process_gender_and_dob_from_NIK_dob(dob)
	except:
		gender_ext,dob_ext = 99,99
	return province_ext,district_exr,subdistrict_ext,gender_ext,dob_ext