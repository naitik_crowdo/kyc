# -*- coding: utf-8 -*-
"""
Created on Mon Aug 20 14:41:29 2018

@author: CWDSG05
"""

import os
#os.chdir(r"C:\naitik\ocr_part1_and_part2\hybrid")
os.chdir("/home/ubuntu/Python_Directory/kyc_app/Code")

#Call ocr related scripts for part2
from ocr.ind_ktp_ocr_with_class_v2 import call_ocr
#call script to call google vision part2
from vision.vision_working_v2 import fetch_vision

#import some utils
from util.script import transform
#import script to download and upload S3 files
from util.s3_connect import s3_conn
#Import script to process passport scan image pipelines
from passport.process_passport import read_mrz

#import counter
from timeit import default_timer as timer
#import flask lib
from flask import Flask, jsonify, abort, make_response, request
#import supporting libs
import cv2, datetime, json
from numpy import int0


#Initiate both part for init call
#p = fetch_vision()
q = call_ocr()
#p = q
p = fetch_vision()
s3 = s3_conn()

##Instantiate the app
app = Flask(__name__)

NOT_FOUND = 'Not found'
BAD_REQUEST = 'Bad request'
NO_IMAGE = 'Image not found at path'
S3_ERROR = 'Problem in Connection with S3'

@app.errorhandler(404)
def not_found(error):
	return make_response(jsonify({'error': NOT_FOUND}), 404)

@app.errorhandler(400)
def bad_request(error):
	return make_response(jsonify({'error': BAD_REQUEST}), 400)

@app.errorhandler(403)
def bad_request_withS3(error):
	return make_response(jsonify({'error': S3_ERROR}), 403)

@app.errorhandler(408)
def image_not_found(error):
	return make_response(jsonify({'error': NO_IMAGE}), 408)

@app.route('/')
def hello_world():
	return make_response(jsonify({'kyc-ocr production': 'server is super-active'}), 200)

@app.route('/test', methods=['GET','POST'])
def test_sum():
	import random
	a=random.randint(1,100)
	b=random.randint(1,100)
	return jsonify({'sum of 2 random numbers': a+b}), 200

###get current timestamp in format for logging purpose
def cur_time(arg1):
	return datetime.datetime.now().strftime(arg1)

####To convert the format for Points received into format acceptable in code
def process_points(points):
	'''Process Points received which is in format of {'0': [1, 2],
													  '1': [2, 3],
													  '2': [3, 4],
													  '3': [4, 5]}
	into
												array([[3, 4],
													   [1, 2],
													   [2, 3],
													   [4, 5]], dtype=int64)'''
	points_process = []
	for k in points:
		points_process.append(points.get(k))
	points_conv = int0(points_process)
	return points_conv

def str2bool(v):
  return v.lower() in ("yes", "true", "t", "1")

@app.route('/api/v1.0/transform', methods=['POST','GET'])
def receive_points_and_image_path_return_image_path_only():
	'''
	RECEIVE: bucket name, Remote file path, 4 points, Ratio
	RESPONSE: upload bucket name, transformed image remote file path

	STEP1: Process the points received in proper format.
	STEP2: Download the file from S3.
	STEP3: Transform the image (image_transform.jpg)
	STEP4: Upload the image to S3 bucket and return updated file path.
	'''
	logname = 'log/log_transform_'+cur_time("%d%m%Y")+'.log'
	#print("data:",request.data)
	#print("json:",request.json)
	angle = 0
	with open(logname,'a+') as f:
		f.write("<<<<<<<<<<<<<<<<<<<<<<<<<<<<Call Transform\n")
		f.write("Starting time-"+cur_time("%H:%M:%S")+'\n')
		f.write('Called the route "/api/v1.0/transform"\n')
		if not request.json or 'points' not in request.json or 'bucket' not in request.json or 'remote_file' not in request.json or 'ratio' not in request.json:
			f.write("request received:{}\n".format(request.data))
			print(request.data)
			f.write('Bad request\n')
			f.write(">>>>>>>>>>>>>>>>>>>>>>>>>>>>\n")
			abort(400)
		#return dict in proper format
		f.write("request received :{}\n".format(request.json))

		myBucketName = request.json['bucket'] #extract bucket name from where image need to download
		remote_imag = request.json['remote_file']	#extract image path from dictionary
		points = request.json['points']  #extract only points data from dictionary
		ratio = request.json['ratio']	#extract the ratio for resize done in image

		############ Get Image rotate angle here
		try:
			angle =  int(request.json.get('angle'))
		except:
			angle = 0

		############ process points here
		f.write("1-start Point processing------------------\n")
		points_processed = process_points(points)
		############ process image part here
		f.write("2-start image processing------------------\n")
		# read the image from path
		try:
			local_file,status = s3.down_frm_s3(remote_imag)
			img = cv2.imread(local_file)
		except:
			f.write("Bad image received\nNot able to download image\n")
			f.write(">>>>>>>>>>>>>>>>>>>>>>>>>>>>\n")
			abort(408)
		f.write("3--------------------------------------------done image and points processing\n")

		# do some fancy processing here....
		f.write("4-calling transform-----------------------\n")
		transform_image = transform(img,points_processed, angle, ratio=float(ratio)) #added rotated angle information also, so image saved will be rotated now

		f.write("5--------------------------------------------done transform\n")
		temp_file_name = "files/"+os.path.basename(remote_imag).split(".")[0]+"_transform.jpg"
		#file = 'temp.jpg'
		cv2.imwrite(temp_file_name,transform_image)
		f.write("Finished transform request at {}\n".format(cur_time("%H:%M:%S")))
		f.write("...Uploading image to S3 bucket\n")
		return_bucket,remote_url,status = s3.upl_to_s3(myBucketName,temp_file_name,remote_imag,file_type = 'image')
		f.write(">>>>>>>>>>>>>>>>>>>>>>>>>>>>\n...Completed Operation...\n")
	return make_response(jsonify({'remote_path': remote_url,'used_bucket':return_bucket}), 200) if status == 200 else abort(403)

############################Part 1 over where image has been transform and sent back to origin
##############################################################################################

####--Part2 Begins
@app.route('/api/v1.0/ocr', methods=['GET','POST'])
def receive_image_name_extract_text_return_json():
	'''
	Take input of S3 bucket name and Remote file path of S3. How it proceeds:
		step1: If Reuse = False, download the transformed image from S3 to local system else just use name only.
		step2: Call Vision service on Downloaded _transformed.jpg file
		step3: Extract groups using same line concept from vision result
		step4: Map the tags known to the line group form earlier
		step5: Write the result json dump into file and upload to S3

	'''
	logname = 'log/log_transform_'+cur_time("%d%m%Y")+'.log'
	with open(logname,'a+') as f:
		f.write("\n<<<<<<<<<<<<<<<<<<<<<<<<<<<<Call OCR Vision\n")
		f.write("Starting time-"+cur_time("%H:%M:%S")+'\n')
		f.write('Called the route "/api/v1.0/ocr"\n')
		if not request.json or 'remote_path' not in request.json or 'Reuse' not in request.json or 'bucket' not in request.json:
			f.write("request received:{}\n".format(request.data))
			print(request.data)
			f.write('Bad request\n')
			f.write(">>>>>>>>>>>>>>>>>>>>>>>>>>>>\n")
			abort(400)

		############ Get the Image name here
		remote_image_name =  request.json.get('remote_path')
		myBucketName =  request.json.get('bucket')

		############ Get Reuse Variable value
		Reuse = str2bool(request.json.get('Reuse'))

		f.write("....Downloading image from S3 to local system\n")
		######Download the image from S3 to local directory for processing
		###### STEP 1
		if Reuse == False:
			try:
				local_image_name,status = s3.down_frm_s3(remote_image_name)
			except:
				abort(403)
		else:
			local_image_name = 'files/'+os.path.basename(remote_image_name).split('.')[0]+'_cp.jpg'

		f.write("....Extracting OCR for Image: {}\n".format(local_image_name))

		############ Initiate a timer to monitor time taken
		start = timer()                   ##Start Timer

		############ Call for Google vision with Image name Input
		###### STEP 2
		f.write("calling Google Vision <<STARTED>>------------------\n")
		##Set Reuse = True if no need to call vision again for same image
		ocr_raw_text = p.call_vision(local_image_name,Reuse)
		ocr_end = timer()                 ##OCR finished timer
		f.write("Vision Call <<FINISHED>>, Time Taken: {}sec\n".format(round(ocr_end-start,2)))

		############ Received the raw input from google vision and send for grouping based on their horizontal position
		###### STEP 3
		f.write("Extracting Information <<STARTED>>\n")
		#text_after_group = p.process_vision_text_to_groups(ocr_raw_text)
		buffer_identified,text_after_group = p.process_vision_text_to_groups(ocr_raw_text)
		f.write("------------ Buffer Identified::\n")
		f.write("Buffer: {}\n".format(buffer_identified))

		f.write("------------Extracted Text::\n")
		f.write(json.dumps(text_after_group))
		#print("Extracted Text :",text_after_group)

		############ After done grouping send to extract information from groups(tries to map the groups to known tags)
		###### STEP 4
		result = q.call_f_ocr(text_after_group)
		ext_end = timer()                 ##extraction end timer

		########### create a dictionary for result file save
		d = {}
		d['result'] = result

		############ Save the result local and upload to S3
		###### STEP 5
		f.write("\n..Writing the result into File..,\n")
		#result_file = 'result/'+os.path.basename(remote_image_name).split('.')[0].split('_')[0]+'.json'
		try:
			if os.path.basename(remote_image_name).split('.')[0].split('_')[-1:] in ['cp','transform','CP','TRANSFORM']:
				result_file = 'result/'+'_'.join(os.path.basename(remote_image_name).split('.')[0].split('_')[:-1])+'.json'
			else:
				result_file = 'result/'+ os.path.basename(remote_image_name).split('.')[0]+'.json'
		except:
			result_file = 'result/'+ os.path.basename(remote_image_name).split('.')[0]+'.json'

		with open(result_file, 'w') as outfile:
			   outfile.write(json.dumps(d))

		####Uploading the result back to S3
		f.write("..Uploading {} to S3..,\n".format(result_file))
		try:
			### uses bucketName(dummy for now), local_file name(result/ktp.json, remote file name(/ocr/id/ktp_transform.jpg, file_type(json in this case)))
			return_bucket,remote_url,status = s3.upl_to_s3(myBucketName,result_file,remote_image_name,file_type = 'json')
		except:
			abort(403)
		### end functinality

		f.write("\nExtraction <<FINISHED>>, Time Taken: {}sec\n".format(round(ext_end-ocr_end,2)))
		f.write("Finished Vision request at {}\n".format(cur_time("%H:%M:%S")))
		f.write(">>>>>>>>>>>>>>>>>>>>>>>>>>>>\n")
		#print('Extraction <<FINISHED>>, Time Taken:',round(ext_end-ocr_end,2),'sec')
	return make_response(jsonify({'result': result}), 200)

####PART -3  begins for handling Passport related images
@app.route('/api/v1.1/ocr_pass', methods=['GET','POST'])
def receive_local_image_path_extract_text_return_string():
	logname = 'log/log_transform_'+cur_time("%d%m%Y")+'.log'
	with open(logname,'a+') as f:
		f.write("\n<<<<<<<<<<<<<<<<<<<<<<<<<<<<Call OCR Vision for Passport\n")
		f.write("Starting time-"+cur_time("%H:%M:%S")+'\n')
		f.write('Called the route "/api/v1.1/ocr_pass"\n')
		if not request.json or 'remote_path' not in request.json or 'Reuse' not in request.json or 'bucket' not in request.json:
			f.write("request received:{}\n".format(request.data))
			print(request.data)
			f.write('Bad request\n')
			f.write(">>>>>>>>>>>>>>>>>>>>>>>>>>>>\n")
			abort(400)

		############ Get the Image name here
		remote_image_name =  request.json.get('remote_path')
		myBucketName =  request.json.get('bucket')

		############ Get Reuse Variable value
		Reuse = str2bool(request.json.get('Reuse'))
		print("Passport Extraction Starts:: {}".format(cur_time("%H:%M:%S")))

		f.write("....Downloading image from S3 to local system\n")
		######Download the image from S3 to local directory for processing
		###### STEP 1
		if Reuse == False:
			try:
				local_image_name,status = s3.down_frm_s3(remote_image_name)
			except:
				abort(403)
		else:
			local_image_name = 'files/'+os.path.basename(remote_image_name).split('.')[0]+'_cp.jpg'

		f.write("....Extracting OCR for Image using tesseract: {}\n".format(local_image_name))
		############ Initiate a timer to monitor time taken
		start = timer()                   ##Start Timer
		############ Call for mrz read pipeline with Image name Input
		text,proc_text = read_mrz(local_image_name,debug = False)
		############ End the timer for mrz reading pipeline
		ocr_end = timer()                 ##OCR finished timer
		f.write("pipeline Call <<FINISHED>>, Time Taken: {}sec\n".format(round(ocr_end-start,2)))
    		######printing out result
		#print('Scanned:\n',text)
		print('processed_text:')
		print(json.dumps(proc_text, indent=1))

		############ Save the result local and upload to S3
		f.write("\n..Writing the result into local File..,\n")
		#x= os.path.basename(remote_image_name).split('.')[0].split('_')
		try:
			if os.path.basename(remote_image_name).split('.')[0].split('_')[-1:] in ['cp','transform','CP','TRANSFORM']:
				result_file = 'result/'+'_'.join(os.path.basename(remote_image_name).split('.')[0].split('_')[:-1])+'.json'
			else:
				result_file = 'result/'+ os.path.basename(remote_image_name).split('.')[0]+'.json'
		except:
			result_file = 'result/'+ os.path.basename(remote_image_name).split('.')[0]+'.json'
		with open(result_file, 'w') as outfile:
			   outfile.write(json.dumps(proc_text))

		####Uploading the result back to S3
		f.write("..Uploading {} to S3..,\n".format(result_file))
		try:
			### uses bucketName(dummy for now), local_file name(result/ktp.json, remote file name(/ocr/id/ktp_transform.jpg, file_type(json in this case)))
			return_bucket,remote_url,status = s3.upl_to_s3(myBucketName,result_file,remote_image_name,file_type = 'json')
		except:
			f.write("*********ERROR in Uploading file to S3*******\n")
			abort(403)
		f.write("....All PROCESSING FINISHED....\n")
	  #### end functinality
		f.write("....processed Text:")
		f.write(json.dumps(proc_text, indent =1))
		f.write("\n....Scanned MRZ::\n{}".format(text))
		f.write("\nFinished Passport MRZ extraction request at {}\n".format(cur_time("%H:%M:%S")))
		f.write(">>>>>>>>>>>>>>>>>>>>>>>>>>>>\n")
	return make_response(jsonify({'result': proc_text}), 200)

if __name__ == '__main__':
	#app.run(host='0.0.0.0', port=5252,debug=True)
	app.run(host='127.0.0.2', port=5252, debug=True)

	'''#for Testing purpose
	remote_image_name = 'ocr/member/27/id_document/KTP-600x416_transform.jpg'
	myBucketName = 'cwd-nexus-dev'
	Reuse= True

	##Download S3 file locally
	local_image_name,status = s3.down_frm_s3(remote_image_name)

	##Calling google vision on image
	ocr_raw_text = p.call_vision(local_image_name,Reuse)

	##Create groups on vision result based on their y-axis with buffer set
	text_after_group = p.process_vision_text_to_groups(ocr_raw_text)

	##tries to map te groups into known tags
	result = q.call_f_ocr(text_after_group)
	'''
