# -*- coding: utf-8 -*-
"""
Created on Tue Jul 24 13:51:58 2018

@author: CWDSG05
"""
###############################################################################
import io
import cv2
import os
import numpy as np
import pickle
import math

# Imports the Google Cloud client library
from google.cloud import vision
from google.cloud.vision import types

class fetch_vision_ocr(object):

	def _extract_box_and_text(self,texts):
	    all_text = []
	    box_text = []
	    #text_blob=''
	    for i,text in enumerate(texts):
	        if i==0:
	            continue
	            #print('\n"{}"'.format(text.description))
	            #text_blob=
	            #all_text.append(text.description)
	            #str_text=str(text.description)
	        else:
	            #vertices = (['({},{})'.format(vertex.x, vertex.y)
	            #                for vertex in text.bounding_poly.vertices])
	            vertices = [[int(vertex.x), int(vertex.y)] for vertex in text.bounding_poly.vertices]
	            box_text.append(vertices)
	            all_text.append(text.description)
	            #print('bounds: {}'.format(','.join(vertices)))
	    return all_text,box_text

	def assign_group_with_Yaxis_new(self,box_text,buffer):
	    '''
	    This function will find elemnst from +- buffer size from group starting position
	    if it's inside buffer then assign to group
	    else start the new group and next elemnts will compare from this group and so on.
	    '''
	    temp_grp=[]
	    for i,box in enumerate(box_text):
	        if i==0: group=0;temp_y= box[0][1];temp_grp.append(0)
	        else:
	            if box[0][1] < buffer+temp_y and  box[0][1] > temp_y-buffer: temp_grp.append(group) #here check if group lies within +- buffer size of group starting position of Y
	            else: group += 1; temp_y = box[0][1]; temp_grp.append(group)
	    return temp_grp

	def merge_same_group_element_new(self,all_text,groups_on_Yaxis,ignore_list):
	    '''
	    This function will take group formation detail from previous function and see if text
	    is from same group then append into temp list and if it's different than previous then
	    append the list into dict and empty list for new group store.
	    '''
	    dd={}; temp_grp = []
	    for i,group in enumerate(groups_on_Yaxis):
	        if i==0:
	            cur_group = group
	            if all_text[i] not in ignore_list: temp_grp.append(all_text[i])
	        else:
	            if group == cur_group:
	                if all_text[i] not in ignore_list: temp_grp.append(all_text[i])
	            else:
	                dd[cur_group] = temp_grp
	                temp_grp=[]
	                cur_group = group
	                if all_text[i] not in ignore_list: temp_grp.append(all_text[i])
	    return dd


	def _brighten_new(self,img,factor):
	    '''
	    New function written to brighten the image by a factor given
	    input : Colored image
	    output: brightened gray image
	    '''
	    if factor < 1.0: factor = factor+1.0    #check factor should always do more bright
	    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY) #convert to gray
	    b = cv2.normalize(img,  img, 0, 255, cv2.NORM_MINMAX) #perform full range of 0-255 for raw image
	    b = factor*b                            #multiply every value in array by a factor
	    b[b > 255] = 255                        #change all those values which cross threshold for gray color
	    b = b.astype(np.uint8)                  #convert to integer
	    gray_img = cv2.normalize(b,  b, 0, 255, cv2.NORM_MINMAX) #again normalize help in more dark areas as minimum value already multiplied
	    return gray_img


class fetch_vision(object):
	def __init__(self):
		self.client = vision.ImageAnnotatorClient()
		self.f = fetch_vision_ocr()
		self.factor = 1.54      #brightness factor
		self.ignore_list=['•','*',":",'=',':']  #punctuation list
		self.buffer = 20        #For building groups in same line
		self.divide_factor = 25		#Factor used to divide the height of image to obtain the buffer

	def call_vision(self,file_name,ReUse=False):
		'''Function to call Goolge Vision Service to extract the OCR container from Google'''
		file_name = os.path.basename(file_name)
		if ReUse == False or os.path.isfile('files/'+file_name.split(".")[0]+'.p') == False:
			print("Calling Google Vision Service")
			file_name_first_prt = file_name.split(".")[0]
			image = cv2.imread('files/'+file_name)
			#Extracting buffer size depend on input image
			self.buffer = int(math.ceil(image.shape[0]/self.divide_factor))
			print("Set buffer = {}".format(self.buffer))
			#brighten the image to  better Google Vision
			gray_image = self.f._brighten_new(image,self.factor)
			cv2.imwrite('files/'+file_name_first_prt+'_to_vision.png',gray_image) #write the temp file in folder to save

			# Loads the image into memory
			with io.open('files/'+file_name_first_prt+'_to_vision.png', 'rb') as image_file:
			#with io.open('ktp_cut.jpg', 'rb') as image_file:
				content = image_file.read()
			image = types.Image(content=content)
			'''actual google vision call happens here'''
			#response = self.client.document_text_detection(image=image)
			response = self.client.document_text_detection(image=image)

			#save the pickle file of google result
			pickle.dump( response, open("files/"+file_name_first_prt+".p", "wb" ) )
		else:
			try:
				print("No need to call google vision Service")
				file_name_first_prt = file_name.split(".")[0]
				response = pickle.load( open("files/"+file_name_first_prt+".p", "rb" ) )
				print("using {} for data extraction".format("files/"+file_name_first_prt+".p"))
				#reading image in memory for shape extraction
				image = cv2.imread('files/'+file_name)
				print("Image read successfully: files/{}, with height: {}".format(file_name,image.shape[0]))
				##getting shape of image for buffer size to adjust based in size of image
				self.buffer = int(math.ceil(image.shape[0]/self.divide_factor))
				print("Set buffer = {}".format(self.buffer))
			except:
				print("Failed in reading Google Vision file or Setting up Buffer")
				return 404

		'''extract only  text annotations from return elements'''
		texts  = response.text_annotations
		return texts

	def process_vision_text_to_groups(self,texts):
		'''Take input of Google OCR and create groups of boxes with limit of buffer'''
		all_text,box_text = self.f._extract_box_and_text(texts)    #Find text and box list from return structure
		#groups_on_Yaxis = assign_group_with_Yaxis(box_text,buffer=20) #find groups on boxes based on Y axis
		groups_on_Yaxis = self.f.assign_group_with_Yaxis_new(box_text,self.buffer)
		text_after_group = self.f.merge_same_group_element_new(all_text,groups_on_Yaxis,self.ignore_list) #merge the groups return in last step with text
		#pprint(text_after_group)
		return text_after_group


if __name__=="__main__":
    from pprint import pprint
    # Instantiates a client
    client = vision.ImageAnnotatorClient()
    f = fetch_vision_ocr() #call the class built

	 # The name of the image file to extract text
    #file_name = 'ktp_KTP_Alo_Baru_3.jpg'
    file_name = 'ktp_cut.jpg'

    image = cv2.imread(file_name)
    gray_image = f._brighten_new(image,1.54)
    cv2.imwrite('temp.png',gray_image)

    buffer_var = int(math.ceil(image.shape[0]/25))

    # Loads the image into memory
    with io.open('temp.png', 'rb') as image_file:
        content = image_file.read()
    image = types.Image(content=content)
    '''actual google vision call happens here'''
    response = client.document_text_detection(image=image)
    '''extract only  text annotations from return elements'''
    texts  = response.text_annotations

    ignore_list=['•','*',":",'=',':']
    all_text,box_text = f._extract_box_and_text(texts)    #Find text and box list from return structure
    #groups_on_Yaxis = assign_group_with_Yaxis(box_text,buffer=20) #find groups on boxes based on Y axis
    groups_on_Yaxis = f.assign_group_with_Yaxis_new(box_text,buffer=buffer_var)
    text_after_group = f.merge_same_group_element_new(all_text,groups_on_Yaxis,ignore_list) #merge the groups return in last step with text
    pprint(text_after_group)
