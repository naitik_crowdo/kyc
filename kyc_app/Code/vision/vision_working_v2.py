# -*- coding: utf-8 -*-
"""
Created on Tue Jul 24 13:51:58 2018

@author: CWDSG05
"""
###############################################################################
import io
import cv2
import os
import numpy as np
import pickle
import math

# Imports the Google Cloud client library
from google.cloud import vision
from google.cloud.vision import types

class fetch_vision_ocr(object):

	def _extract_box_and_text(self,texts):
	    all_text = []
	    box_text = []
	    #text_blob=''
	    for i,text in enumerate(texts):
	        if i==0:
	            continue
	            #print('\n"{}"'.format(text.description))
	            #text_blob=
	            #all_text.append(text.description)
	            #str_text=str(text.description)
	        else:
	            #vertices = (['({},{})'.format(vertex.x, vertex.y)
	            #                for vertex in text.bounding_poly.vertices])
	            vertices = [[int(vertex.x), int(vertex.y)] for vertex in text.bounding_poly.vertices]
	            box_text.append(vertices)
	            all_text.append(text.description)
	            #print('bounds: {}'.format(','.join(vertices)))
	    return all_text,box_text

	''' New Function added in v2 on 10-09 to find buffer value based on  average of y-axis(height) of boxes found in 10-18 position(hopefully after Name tag)'''
	def find_buffer_value(self,box_text):
		poss_diff = []
		for i in range(10,18):
			poss_diff.append(abs(box_text[i][1][1] - box_text[i][2][1])) #find diff between 2nd and 3rd box y value
		self.buffer = sum(poss_diff)//len(poss_diff)
		return self.buffer


	#def assign_group_with_Yaxis_new(self,box_text,buffer):

	    #This function will find element from +- buffer size from group starting position
	    #if it's inside buffer then assign to group
	    #else start the new group and next elemnts will compare from this group and so on.

	    #temp_grp=[]
	    #for i,box in enumerate(box_text):
	    #    if i==0: group=0;temp_y= box[0][1];temp_grp.append(0)
	    #    else:
	    #        if box[0][1] < buffer+temp_y and  box[0][1] > temp_y-buffer: temp_grp.append(group) #here check if group lies within +- buffer size of group starting position of Y
	    #        else: group += 1; temp_y = box[0][1]; temp_grp.append(group)
	    #return temp_grp


	def assign_group_with_Yaxis_new(self,box_text,buffer):
		''' 
		New function written to find the groups even with sometimes unordered text box returned from google vision, Also uses the dynamic buffer now from the text height
		'''
		final_grp = []
		for i,box in enumerate(box_text):
			possible_grp = []
			this_y = box[0][1] #current y value from all box
			if i==0: #means no group is there first element
				cur_group =0 #initialise group
				final_grp.append([cur_group,this_y])
			else: ##means some group is already there
				for grp in final_grp: #go through all the finalized groups
					other_y = grp[1]
					other_grp = grp[0]
					if this_y < other_y+buffer and this_y > other_y-buffer: #means current y belongs to other group came earlier
						if not possible_grp: #and currently this is first entry match for possible group
							possible_grp.append([other_grp,other_y]) #append matched group number, and y-coord and distance between this_y and matched y coord.
						else: #means some other entry also present inside the possible groups but this_y is in range of other_y also, so check first if entry in possible group is different from other_grp(check for any group only for first instance) and if it's difference check for their y difference and use smaller one only in possible group
							if possible_grp[0][0] == other_grp: #if prev grp and current grp is same leave this append
								continue
							else:
								dist_possible_grp_entry = abs(possible_grp[0][1]-this_y)
								dist_other_y = abs(other_y-this_y)
								if dist_other_y < dist_possible_grp_entry: #means previous possible group was far from other_grp identified
									possible_grp[0] = [other_grp,other_y] #replace the previous entry with new entry
				##this condition is for if no previous group found so it will create new group of it's own
				if not possible_grp: cur_group += 1; possible_grp.append([cur_group,this_y])
				final_grp.append(possible_grp[0])

		##Extraction from final group list for return format
		to_return_group_list=[]
		for grp in final_grp:
			to_return_group_list.append(grp[0])
		return to_return_group_list

	''' 
	def merge_same_group_element_new(self,all_text,groups_on_Yaxis,ignore_list):
	'''
	''' This function will take group formation detail from previous function and see if text
	    is from same group then append into temp list and if it's different than previous then
	    append the list into dict and empty list for new group store.
	'''
	''' dd={}; temp_grp = []
	    for i,group in enumerate(groups_on_Yaxis):
	        if i==0:
	            cur_group = group
	            if all_text[i] not in ignore_list: temp_grp.append(all_text[i])
	        else:
	            if group == cur_group:
	                if all_text[i] not in ignore_list: temp_grp.append(all_text[i])
	            else:
	                dd[cur_group] = temp_grp
	                temp_grp=[]
	                cur_group = group
	                if all_text[i] not in ignore_list: temp_grp.append(all_text[i])
	    return dd
	'''

	def merge_same_group_element_updated(self,all_text,groups_on_Yaxis,ignore_list):
		''' 
	    	This function will take group formation detail from previous function and sort the list based on group receive, see if text is from same group then append into temp list and if it's different than previous then
	    	append the list into dict and empty list for new group store.
	    	'''
		list3 = [list(a) for a in zip(groups_on_Yaxis, all_text)] #merge two list together
		list3_sorted = sorted(list3,key=lambda x: (x[0])) #sort the merge list based on group(first value)

		dd={} #initialize the dictionary to return
		cur_i = list3_sorted[0][0] #initialize the variable for storing the group
		temp_y = [] #initialize the list for storing text for a group
		for i in list3_sorted: #iterate over the sorted list newly created
			if i[0] == cur_i and i[1] not in ignore_list: #looking for text not in ignore list
				temp_y.append(i[1]) #current group only so need to append
			elif i[0] != cur_i and i[1] not in ignore_list: #it means some new group arrive in sort order
				dd[cur_i] = temp_y
				cur_i = i[0]
				temp_y=[]
				temp_y.append(i[1]) #append with new group detail
		return dd

	def _brighten_new(self,img,factor):
	    '''
	    New function written to brighten the image by a factor given
	    input : Colored image
	    output: brightened gray image
	    '''
	    if factor < 1.0: factor = factor+1.0    #check factor should always do more bright
	    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY) #convert to gray
	    b = cv2.normalize(img,  img, 0, 255, cv2.NORM_MINMAX) #perform full range of 0-255 for raw image
	    b = factor*b                            #multiply every value in array by a factor
	    b[b > 255] = 255                        #change all those values which cross threshold for gray color
	    b = b.astype(np.uint8)                  #convert to integer
	    gray_img = cv2.normalize(b,  b, 0, 255, cv2.NORM_MINMAX) #again normalize help in more dark areas as minimum value already multiplied
	    return gray_img


class fetch_vision(object):
	def __init__(self):
		self.client = vision.ImageAnnotatorClient()
		self.f = fetch_vision_ocr()
		self.factor = 1.54      #brightness factor
		self.ignore_list=['•','*',":",'=',':']  #punctuation list
		self.buffer = 20        #For building groups in same line
		self.divide_factor = 25		#Factor used to divide the height of image to obtain the buffer

	def call_vision(self,file_name,ReUse=False):
		'''Function to call Goolge Vision Service to extract the OCR container from Google'''
		file_name = os.path.basename(file_name)
		if ReUse == False or os.path.isfile('files/'+file_name.split(".")[0]+'.p') == False:
			print("Calling Google Vision Service")
			file_name_first_prt = file_name.split(".")[0]
			image = cv2.imread('files/'+file_name)
			#Extracting buffer size depend on input image
			self.buffer = int(math.ceil(image.shape[0]/self.divide_factor))
			print("Set buffer = {}".format(self.buffer))
			#brighten the image to  better Google Vision
			gray_image = self.f._brighten_new(image,self.factor)
			cv2.imwrite('files/'+file_name_first_prt+'_to_vision.png',gray_image) #write the temp file in folder to save

			# Loads the image into memory
			with io.open('files/'+file_name_first_prt+'_to_vision.png', 'rb') as image_file:
			#with io.open('ktp_cut.jpg', 'rb') as image_file:
				content = image_file.read()
			image = types.Image(content=content)
			'''actual google vision call happens here'''
			#response = self.client.document_text_detection(image=image)
			response = self.client.document_text_detection(image=image)

			#save the pickle file of google result
			pickle.dump( response, open("files/"+file_name_first_prt+".p", "wb" ) )
		else:
			try:
				print("No need to call google vision Service")
				file_name_first_prt = file_name.split(".")[0]
				response = pickle.load( open("files/"+file_name_first_prt+".p", "rb" ) )
				print("using {} for data extraction".format("files/"+file_name_first_prt+".p"))
				#reading image in memory for shape extraction
				image = cv2.imread('files/'+file_name)
				print("Image read successfully: files/{}, with height: {}".format(file_name,image.shape[0]))
				##getting shape of image for buffer size to adjust based in size of image
				self.buffer = int(math.ceil(image.shape[0]/self.divide_factor))
				print("Set buffer = {}".format(self.buffer))
			except:
				print("Failed in reading Google Vision file or Setting up Buffer")
				return 404

		'''extract only  text annotations from return elements'''
		texts  = response.text_annotations
		return texts

	def process_vision_text_to_groups(self,texts):
		'''Take input of Google OCR and create groups of boxes with limit of buffer'''
		all_text,box_text = self.f._extract_box_and_text(texts)    #Find text and box list from return structure
		#groups_on_Yaxis = assign_group_with_Yaxis(box_text,buffer=20) #find groups on boxes based on Y axis
		self.buffer = self.f.find_buffer_value(box_text) #extract the buffer information from box received
		groups_on_Yaxis = self.f.assign_group_with_Yaxis_new(box_text,self.buffer)
		#text_after_group = self.f.merge_same_group_element_new(all_text,groups_on_Yaxis,self.ignore_list) #previous version
		text_after_group = self.f.merge_same_group_element_updated(all_text,groups_on_Yaxis,self.ignore_list) #merge the groups return in last step with text
		#pprint(text_after_group)
		return self.buffer,text_after_group

	def draw_poly(self,inputfile,texts):
		''' 
		New function created to create the bounding boxes of the vision result onto the image and save it back
		'''
		from PIL import Image, ImageDraw
		im = Image.open(inputfile)
		draw = ImageDraw.Draw(im)
		for i,text in enumerate(texts):
			vects = text.bounding_poly.vertices
			draw.polygon([
			        vects[0].x, vects[0].y,
			        vects[1].x, vects[1].y,
			        vects[2].x, vects[2].y,
			        vects[3].x, vects[3].y], None, 'red')
			draw.text((vects[0].x, vects[0].y), str(i), 'black',size=12)
		im.save('files/'+inputfile.split('.')[0]+'_box.jpg', 'JPEG')



if __name__=="__main__":
    from pprint import pprint
    # Instantiates a client
    client = vision.ImageAnnotatorClient()
    f = fetch_vision_ocr() #call the class built

	 # The name of the image file to extract text
    #file_name = 'ktp_KTP_Alo_Baru_3.jpg'
    file_name = 'files/ktp_cut.jpg'

    image = cv2.imread(file_name)
    gray_image = f._brighten_new(image,1.54)
    cv2.imwrite('files/temp.png',gray_image)

    buffer_var = int(math.ceil(image.shape[0]/25))

    # Loads the image into memory
    with io.open('temp.png', 'rb') as image_file:
        content = image_file.read()
    image = types.Image(content=content)
    '''actual google vision call happens here'''
    response = client.document_text_detection(image=image)
    '''extract only  text annotations from return elements'''
    texts  = response.text_annotations

    #optional to draw bounding box in image to see real action
    #draw_poly(file_name,texts)

    ignore_list=['•','*',":",'=',':']
    all_text,box_text = f._extract_box_and_text(texts)    #Find text and box list from return structure
    #groups_on_Yaxis = assign_group_with_Yaxis(box_text,buffer=20) #find groups on boxes based on Y axis
    buffer = f.find_buffer_value(box_text)
    print("buffer identified:",buffer)
    groups_on_Yaxis = f.assign_group_with_Yaxis_new(box_text,buffer=buffer)
    text_after_group = f.merge_same_group_element_updated(all_text,groups_on_Yaxis,ignore_list) #merge the groups return in last step with text

    #groups_on_Yaxis = f.assign_group_with_Yaxis_new(box_text,buffer=buffer_var)
    #text_after_group = f.merge_same_group_element_new(all_text,groups_on_Yaxis,ignore_list) #merge the groups return in last step with text
    pprint(text_after_group)
