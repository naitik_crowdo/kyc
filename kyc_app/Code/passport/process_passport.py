# -*- coding: utf-8 -*-
"""
Created on Tue Jun 12 13:21:54 2018

@author: CWDSG05
"""
#import sys
#del sys.modules['analyze_MRZ']
#reload(analyze_MRZ)
#from geometry import RotatedBox

#from skimage import transform, io, morphology, filters, measure
from skimage import morphology, filters
from util.tesseract_ocr import ocr
import numpy as np
#from geometry import RotatedBox
import cv2, imutils
from util.pipeline import Pipeline
#import pytesseract
from skimage.filters import threshold_local
from util.analyze_MRZ import MRZ_p

#from passport.transform import four_point_transform
'''
            d['TYPE']= self.mrz_type
            d['SCORE']= self.valid_score
            d['ID']= self.number
            d['FIRST_NAME']= self.names
            d['SURNAME']= self.surname
            d['GENDER']= self.sex
            d['DOB']= self.date_of_birth
            d['EXPIRY']= self.expiration_date
			'''
class read_text(object):
    __depends__ = ['MRZ']
    __provides__ = ['MRZ_text','MRZ_text_process']

    def __init__(self,debug):
        self.kernel = np.ones((2,2),np.uint8)
        self.debug = debug

    def __call__(self,MRZ):
        MRZ_image=MRZ
        MRZ_gray = cv2.cvtColor(MRZ_image, cv2.COLOR_BGR2GRAY)     #converting to grayscale
        MRZ_gray = cv2.GaussianBlur(MRZ_gray, (5, 5), 0)                #doing  little gaussian blur to smooth the edges
        #T = cv2.threshold(MRZ_gray,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)[1]         #very bad
        #T = cv2.adaptiveThreshold(MRZ_gray,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY,11,2)
        T = threshold_local(MRZ_gray, 11, offset = 10, method = "gaussian")      #doing Local gaussian thresholding
        MRZ_bin = (MRZ_gray < T).astype("uint8") * 255
        MRZ_bin = cv2.morphologyEx(MRZ_bin, cv2.MORPH_CLOSE, self.kernel)
        #MRZ_bin = cv2.morphologyEx(MRZ_bin, cv2.MORPH_OPEN, None)
        '''Running tesseract call '''
        #MRZ_text = pytesseract.image_to_string(MRZ_bin, lang = 'eng+math')
        MRZ_text_ocr = ocr(MRZ_bin)
        print("Sending tesseract read to analyze:\n----------------------------")
        '''Send the ocr return text for further cleaning'''
        mrz_text_process = MRZ_p.from_ocr(MRZ_text_ocr)
        print("Text extracted successfully, with Status:",mrz_text_process)
        d = {} #dictionary for processed texts
        if str(mrz_text_process) == 'VALID':
            d['TYPE']= mrz_text_process.mrz_type
            d['SCORE']= 100
            d['ID']= mrz_text_process.number
            d['FIRST_NAME']= mrz_text_process.names
            d['SURNAME']= mrz_text_process.surname
            d['GENDER']= mrz_text_process.sex
            d['DOB']= mrz_text_process.date_of_birth
            d['EXPIRY']= mrz_text_process.expiration_date
        elif str(mrz_text_process) == 'SCORED':
            d['TYPE']= mrz_text_process.mrz_type
            d['SCORE']= mrz_text_process.valid_score
            d['ID']= mrz_text_process.number
            d['FIRST_NAME']= mrz_text_process.names
            d['SURNAME']= mrz_text_process.surname
            d['GENDER']= mrz_text_process.sex
            d['DOB']= mrz_text_process.date_of_birth
            d['EXPIRY']= mrz_text_process.expiration_date
        else: d['TYPE']= 'UNSUCCESSFULL'
        #print(d)

        if self.debug:
            cv2.imshow("before OCR", MRZ_bin)
            cv2.waitKey(0)
            cv2.destroyAllWindows()
            print("OCR read text: \n",MRZ_text_ocr)
            print("Processed Text after reading:\n",mrz_text_process)

        #print("Normal result",MRZ_text)
        return MRZ_text_ocr,d


class MRZBoxLocator(object):
    """Extracts putative MRZs as RotatedBox instances from the contours of `img_binary`"""

    __depends__ = ['img_binary','image_orig']
    __provides__ = ['MRZ','img_crop_width','img_crop_height','mrz_height']

    def __init__(self,debug, thickness=2):
        self.sqKernel = cv2.getStructuringElement(cv2.MORPH_RECT, (21, 21))
        self.thickness = thickness
        self.percent = 0.03
        self.debug = debug

    def __call__(self,image_binary,image_orig):
        ''' find contours in the thresholded image and sort them by their size'''
        cnts = cv2.findContours(image_binary, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        cnts = cnts[0] if imutils.is_cv2() else cnts[1]
        cnts = sorted(cnts, key=cv2.contourArea, reverse=True)

        ''' Loop over contours and select only those contours where ar(ratio) > 5 and width ratio around 75% of image width'''
        for c in cnts :
            orig = image_orig.copy()
            ##Try to fill the shape of detected contour
            rect = cv2.minAreaRect(c) #returns a Box2D structure which contains following detals - ( center (x,y), (width, height), angle of rotation )
            box = cv2.boxPoints(rect)
            box = np.int0(box)

            cv2.drawContours(orig,[box],0,(0,0,255),2)              #min box shape contour

            (x, y, w, h) = cv2.boundingRect(box)                    #found the coordinate of bounding rectangle
            cv2.rectangle(orig, (x, y), (x + w, y + h), (0, 255, 0), 2) #draw rectangle found

            ar = w / float(h)                                        #Width by height ratio of box
            crWidth = w / float(orig.shape[1])                  # width of box to width of image

            ''' check to see if the aspect ratio and coverage width are within acceptable criteria '''
            if ar > 5 and crWidth > 0.75:
                '''Expand the contour identified to include whole data '''
                x,y,w,h = self._expand_box(x,y,w,h,self.percent)
                '''
                pX = int((x + w) * 0.03)
                pY = int((y + h) * 0.03)
                (x, y) = (x - pX, y - pY)
                (w, h) = (w + (pX * 2), h + (pY * 2))
                '''
                MRZ = image_orig[y:y + h, x:x + w].copy()     #Cropping the area identified by contour
                cv2.rectangle(orig, (x, y), (x + w, y + h), (255, 255, 0), self.thickness) ##plot the final rectangle expanded version
                break                           # This is important because if once found the area just exit from other contours search

        if self.debug:
            cv2.imshow("Machine readable Zone", MRZ)  #showing cropped area identified as MRZ
            cv2.waitKey(0)
            cv2.destroyAllWindows()
            cv2.imwrite('files/MRZ_cut.png',MRZ)

        img_crop_width = image_orig.shape[1]
        img_crop_height = image_orig.shape[0]
        mrz_height = y
        return MRZ,img_crop_width,img_crop_height,mrz_height  #mrz photo,width,height,mrz_height

    @staticmethod
    def _expand_box(x,y,w,h,*args, **kwargs):
        Xpercent = kwargs.get('Xpercent', .03)
        Ypercent = kwargs.get('Ypercent', .03)
        pX = int((x + w) * Xpercent)
        pY = int((y + h) * Ypercent)
        (x, y) = (x - pX, y - pY)
        (w, h) = (w + (pX * 2), h + (pY * 2))
        return x,y,w,h


class BooneTransform(object):
    ''' All morphological transformation and return binary image to identify MRZ'''
    __depends__ = ['img_trans','orig_resize_img_width']
    __provides__ = ['img_binary']

    def __init__(self, debug, square_size=5,img_width = 800):
            self.square_size = square_size
            self.rectKernel = [10,5]
            #self.rectKernel = cv2.getStructuringElement(cv2.MORPH_RECT, (14, 6))
            #self.sqKernel = cv2.getStructuringElement(cv2.MORPH_RECT, (21, 21))
            self.sqKernel = [5,5]
            self.debug = debug

    def __call__(self,img_gray,img_width):
            #sq1Kernel = morphology.square(self.square_size)
						##Set the kernel based on input image width
            self.rectKernel = cv2.getStructuringElement(cv2.MORPH_RECT, (int(img_width/(500/14)), int(img_width/(500/6))))
            self.sqKernel = cv2.getStructuringElement(cv2.MORPH_RECT, (int(img_width/(500/21)), int(img_width/(500/21))))

            img_th = morphology.black_tophat(img_gray, self.rectKernel)
            ''' compute the Scharr gradient of the black_tophat image and scale the result into the range [0, 255]'''
            img_sob = abs(filters.sobel_v(img_th))
            (minVal, maxVal) = (np.min(img_sob), np.max(img_sob))
            img_sob = (255 * ((img_sob - minVal) / (maxVal - minVal))).astype("uint8")
            ''' apply a closing operation using the rectangular kernel to close gaps in between letters
                then apply Otsu's thresholding method'''
            img_closed = cv2.morphologyEx(img_sob, cv2.MORPH_CLOSE, self.rectKernel)
            img_binary = cv2.threshold(img_closed, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]
            #img_closed = morphology.closing(img_sob, self.rectKernel)
            #threshold = filters.threshold_otsu(img_closed)
            ''' perform another closing operation, this time using the square
                kernel to close gaps between lines of the MRZ, then perform a
                series of erosions to break apart connected components'''
            img_binary = cv2.morphologyEx(img_binary, cv2.MORPH_CLOSE, self.sqKernel)
            img_binary = cv2.erode(img_binary, None, iterations=4)
            '''  during thresholding, it's possible that border pixels were
                 included in the thresholding, so let's set 5% of the left and
                 right borders to zero '''
            p = int(img_width * 0.05)
            img_binary[:, 0:p] = 0
            img_binary[:, img_width - p:] = 0

            ''' Only show when debug mode is on'''
            if self.debug:
                cv2.imshow("binary", img_binary)
                cv2.waitKey(0)
                cv2.destroyAllWindows()

            return img_binary

class medium_bw_source_2_boone(object):
	__depends__ = []
	__provides__ = ['img_trans','image_orig','orig_resize_img_width']

	def __init__(self, img_path):
		self.image = cv2.imread(img_path)

	def __call__(self):
		try:
			orig_resize_img_width = self.image.shape[1]
		except:
			print("Please check file path, Not read correctly")
			exit
		img_trans = cv2.cvtColor(self.image.copy(), cv2.COLOR_BGR2GRAY)
		### provides trans_image(greyscale image), Original Image, and Image width for Transform
		return img_trans, self.image, orig_resize_img_width



class MRZPipeline(Pipeline):
    """This is the "currently best-performing" pipeline for parsing MRZ from a given image file."""

    def __init__(self, filename, debug=False):
        super(MRZPipeline, self).__init__()
        self.version = '1.0'  # In principle we might have different pipelines in use, so possible backward compatibility is an issue
        self.filename = filename
        self.debug = debug
        self.add_component('connector', medium_bw_source_2_boone(filename))
        self.add_component('boone', BooneTransform(self.debug))
        self.add_component('box_locator', MRZBoxLocator(self.debug))
        self.add_component('read MRZ text', read_text(self.debug))

    @property
    def text(self):
        return self['MRZ_text']

    @property
    def processed_text(self):
        return self['MRZ_text_process']

def read_mrz(filename, debug = False, save_roi=False):
    """The main interface function to this module, encapsulating the recognition pipeline.
       Given an image filename, runs MRZPipeline on it, returning the parsed MRZ object.
    :param save_roi: when this is True, the .aux['roi'] field will contain the Region of Interest where the MRZ was parsed from.
    """
    p = MRZPipeline(filename,debug)
    #img = p.image
    txt = p.text
    p_txt = p.processed_text

    #if img is not None:
        #if save_roi: img.aux['roi'] = p['roi']
    #return img,txt,p_txt
    return txt,p_txt

if __name__=="__main__":
    #filename=r"C:\Users\CWDSG05\Desktop\KYC\indonesia_pass.JPG"
    filename=r"C:\Users\CWDSG05\Desktop\KYC\sing_pass.JPG"
    #face,text,proc_text = read_mrz(filename,debug = False)
    text,proc_text = read_mrz(filename,debug = False)
    print('Scanned:\n',text)
    print('processed_text:\n',proc_text)
    '''if type(face) != 'int' :
        cv2.imshow('Crop_Face',face)
        cv2.waitKey(0)
        cv2.destroyAllWindows()'''

    orig_text = 'PASGPCHAI<<ZHI<XI0NG<<<<<<<<<<<<<<<<<<<<<<<<<\nE5316192F6SGP6410072M2001193S1633136H<<<<<26'
    print('Original:\n',orig_text)



